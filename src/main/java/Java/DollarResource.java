/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Java;

import java.io.IOException;
import java.util.Date;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import java.io.StringReader;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;



/**
 *
 * @author carlos
 */
@Path("/")

public class DollarResource {
    private final String token = "9e8c807c43b8fbac7fccf274eeee15282d80d31d";

        public String getDollarToday () throws WebApplicationException, IOException{
           Client client = ClientBuilder.newClient();
           LocalDate sysDate = java.time.LocalDate.now();
           String url = "https://api.sbif.cl/api-sbifv3/recursos_api/dolar/" + formatDate(sysDate.toString()) +"?formato=JSON&apikey=" + token;
           Response apiResponse = client.target(url).request().get();
        

            String info = "Informacion obtenida de la Super Intendencia de Bancos e Instituciones Financieras (SBIF)";
            String errorText = "";
            String StringUserDate = "";
            String responseData = apiResponse.readEntity(String.class);
  
   
            while (responseData.contains("404")){
                 sysDate = sysDate.minusDays(1);
                 errorText = "{'info': 'No hay informacion en fecha consultada. Se muestra valor mas cercano encontrado'}";
                 StringUserDate = formatDate(sysDate.toString());

                 url = "https://api.sbif.cl/api-sbifv3/recursos_api/dolar/" + StringUserDate   +"?formato=JSON&apikey=" + token;
                 apiResponse = client.target(url).request().get();
                 responseData = apiResponse.readEntity(String.class);
           }   
            
           return url;
        
        }
    
    

    @GET 
    @Path("/dolar/{date}")    
    public String getDollarByDate  (@PathParam("date") String stringDate) throws WebApplicationException, IOException
    {
        Client client = ClientBuilder.newClient();
      
        String formattedDate = formatDate(stringDate);
        
        String url = "https://api.sbif.cl/api-sbifv3/recursos_api/dolar/" + formattedDate +"?formato=JSON&apikey=" + token;
        
        Response apiResponse = client.target(url).request().get();
        

       String info = "Informacion obtenida de la Super Intendencia de Bancos e Instituciones Financieras (SBIF)";
       String errorText = "";
       String StringUserDate = "";
           
       String responseData = apiResponse.readEntity(String.class);
        
       LocalDate userDate = LocalDate.parse(stringDate);
       LocalDate sysDate = java.time.LocalDate.now();
       
       //si fecha ingresada es menor a fecha actual - válida
       if (userDate.compareTo(sysDate) <= 0) {
            while (responseData.contains("404")){
                 errorText = "{'info': 'No hay informacion en fecha consultada. Se muestra valor mas cercano encontrado'}";
                 userDate = userDate.minusDays(1);

                 StringUserDate = formatDate(userDate.toString());

                 url = "https://api.sbif.cl/api-sbifv3/recursos_api/dolar/" + StringUserDate   +"?formato=JSON&apikey=" + token;
                 apiResponse = client.target(url).request().get();
                 responseData = apiResponse.readEntity(String.class);
           }
            
       }else{
             errorText = "{'info': 'Fecha invalida. Se muestra fecha actual o mas cercana'}";
             userDate = sysDate;

              while (responseData.contains("404")){
                 userDate = userDate.minusDays(1);

                 StringUserDate = formatDate(userDate.toString());

                 url = "https://api.sbif.cl/api-sbifv3/recursos_api/dolar/" + StringUserDate   +"?formato=JSON&apikey=" + token;
                 apiResponse = client.target(url).request().get();
                 responseData = apiResponse.readEntity(String.class);
           }
           
       
       }
 
        return info + errorText + responseData;
        
       
    }

    private String formatDate (String date) {
    
        String arrayDate[] = date.split("-");
        
        return arrayDate[0] +"/" + arrayDate[1] + "/dias/" + arrayDate[2] + "/";
    }
}
