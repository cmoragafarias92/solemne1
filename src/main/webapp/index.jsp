<%-- 
    Document   : index
    Created on : Sep 28, 2019, 12:06:15 AM
    Author     : carlos
--%>

<%@page import="javax.ws.rs.core.Response"%>
<%@page import="Java.DollarResource"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html><html>
<%
    DollarResource dollarInstance = new DollarResource();
    String url = dollarInstance.getDollarToday();


%>
    
    <head>
    <meta charset="UTF-8">

    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <title>Dólar</title>
    <style>
        .sticky-bottom{
            bottom: 0;
        }
    </style>
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
    <div class="container">
    <a class="navbar-brand" href="#">Dolar</a>
    </div>
</nav>
<div class="container">
    <div class="jumbotron">
        <h1 class="display-4">Dolar Hoy</h1>
        <p class="lead">Utilizando esta herramienta puedes obtener el valor del Dolar actualizado</p>
        <hr class="my-4">
        <p>Este aplicativo funciona a través de la API desarrollada por la Superintendencia de Bancos e Instituciones Financieras, por lo que permite un valor certero, tanto en su valor actual, como en la fecha indicada.</p>
        <a class="btn btn-primary btn-lg" href="<%=url%>" role="button">Consulta el Dolar de Hoy!</a>
    </div>
</div>

<div class="jumbotron">
        <div class="container">
            <h1 class="display-4">Documentaci&oacute;n</h1>
            <p class="lead">El recurso D&oacute;lar de EE.UU. permite acceder a la informaci&oacute;n almacenada en la base de datos
                del sitio web de la Superintendencia de Bancos e Instituciones Financieras.<br><br>
                Dentro de la url se debe incluir la fecha en su contenido, por lo que se debe escribir el año, el mes y el día,
                separado por guiones medios, para obtener la informaci&oacute;n. En cada caso, la inclusi&oacute;n se hace de la siguiente manera:<br><br>
                Year: se deben incluir cuatro n&uacute;meros del año en el formato AAAA<br>
                Month: se deben incluir dos n&uacute;meros del mes en el formato MM<br>
                Day: se deben incluir dos n&uacute;meros del día en el formato DD<br><br>
                Ejemplo: /api/dolar/2019-02-05
            </p>
        </div>
    </div>
</div>    
        
    
    
<footer class="pt-4 my-md-5 pt-md-5 border-top sticky-bottom">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md">

                <img class="mx-auto d-block" src="https://www.universidadesonline.cl/logos/original/logo-instituto-de-ciencias-tecnologicas-ciisa.png" alt="" width="110" height="110">
                <small class="d-block mb-3 text-muted text-center">Solemne 1 Sección 6</small>
                <small class="d-block mb-3 text-muted text-center">Prof. David Enamorado</small>
                <small class="d-block mb-3 text-muted text-center">Taller de Desarrollo de Aplicaciones Empresariales</small>
            </div>

            <div class="col-6 col-md">
                <h5></h5>
                <ul class="list-unstyled text-small">
                    <li><a class="text-muted" href="#"></a></li>
                    <li><a class="text-muted" href="#"></a></li>
                </ul>
            </div>
            <div class="col-6 col-md">
                <h5></h5>
                <ul class="list-unstyled text-small">
                    <li><a class="text-muted" href="#"></a></li>
                    <li><a class="text-muted" href="#"></a></li>
                </ul>
            </div>
            <div class="col-6 col-md">
                <h5>Integrantes</h5>
                <ul class="list-unstyled text-small">
                    <li><a class="text-muted" href="#">Carlos Moraga</a></li>
                    <li><a class="text-muted" href="#">Luciano Rojo</a></li>
                   
                </ul>
            </div>
        </div>
    </div>
</footer>
</body>
</html>
